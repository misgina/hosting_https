
<?php if ($this->https_enabled && $this->https_key) : ?>

<?php
$satellite_mode = drush_get_option('satellite_mode');
if (!$satellite_mode && $server->satellite_mode) {
  $satellite_mode = $server->satellite_mode;
}

$nginx_has_http2 = drush_get_option('nginx_has_http2');
if (!$nginx_has_http2 && $server->nginx_has_http2) {
  $nginx_has_http2 = $server->nginx_has_http2;
}

if ($nginx_has_http2) {
  $ssl_args = "ssl http2";
}
else {
  $ssl_args = "ssl";
}

$custom_domains = '';
$churchdesk_com_domains = '';
$kw01_net_domains = '';
function domainSupportsIPv6($host) {
  $dns6 = dns_get_record($host, DNS_AAAA);
  if (count($dns6) > 0) return true;
  return false;
}

$this->aliases[] = $this->uri;
foreach ($this->aliases as $alias_url) {
  if (trim($alias_url) && (strpos($alias_url, 'churchdesk.com') === FALSE && strpos($alias_url, 'kw01.net') === FALSE)) {
    // Letsencrypt
    if (in_array(gethostbyname($alias_url), array('88.198.28.198')) && !domainSupportsIPv6($alias_url)) {
      $custom_domains .= " " . str_replace('/', '.', $alias_url);
    }
  } else if (trim($alias_url) && (strpos($alias_url, 'churchdesk.com') !== FALSE && strpos($alias_url, 'www.') === FALSE)) {
    // churchdesk.com
     $churchdesk_com_domains .= " " . str_replace('/', '.', $alias_url);
  } else if(trim($alias_url) && (strpos($alias_url, 'kw01.net') !== FALSE && strpos($alias_url, 'www.') === FALSE)) {
    // kw01.net
     $kw01_net_domains .= " " . str_replace('/', '.', $alias_url);
  }
}

?>

<?php if ($this->redirection): ?>
<?php if(!empty($custom_domains)): ?>
# Redirect all custom domains to the provided redirection domain.
server {
  listen       <?php print "*:{$https_port} {$ssl_args}"; ?>;
<?php
  $server_names = preg_replace("/(?:^|\s)". preg_quote($this->redirection) . "(?:\s|$)/", ' ', $custom_domains);
  print "  server_name  {$server_names};\n";
?>
  ssl                        on;
  ssl_certificate_key        <?php print $https_cert_key; ?>;
<?php if (!empty($https_chain_cert)) : ?>
  ssl_certificate            <?php print $https_chain_cert; ?>;
<?php else: ?>
  ssl_certificate            <?php print $https_cert; ?>;
<?php endif; ?>
  return 301 $scheme://<?php print $this->redirection; ?>$request_uri;
}
<?php endif;?>

<?php if(!empty($churchdesk_com_domains)):?>
# Redirect all churchdesk.com domains to the provided redirection domain.
server {
  listen       <?php print "*:{$https_port} {$ssl_args}"; ?>;
<?php
  $server_names = preg_replace("/(?:^|\s)". preg_quote($this->redirection) . "(?:\s|$)/", ' ', $churchdesk_com_domains);
  print "  server_name  {$server_names};\n";
?>
  ssl_certificate            /var/aegir/platforms/ssl/churchdesk.com/openssl_churchdesk_nginx.crt;
  ssl_certificate_key        /var/aegir/platforms/ssl/churchdesk.com/openssl.key;
  return 301 $scheme://<?php print $this->redirection; ?>$request_uri;
}
<?php endif;?>

<?php if(!empty($kw01_net_domains)):?>
# Redirect all kw01.net domains to the provided redirection domain.
server {
  listen       <?php print "*:{$https_port} {$ssl_args}"; ?>;
<?php
  $server_names = preg_replace("/(?:^|\s)". preg_quote($this->redirection) . "(?:\s|$)/", ' ', $kw01_net_domains);
  print "  server_name  {$server_names};\n";
?>
  ssl                        on;
  ssl_certificate            /var/aegir/platforms/ssl/kw01.net/openssl_kw01_nginx.crt;
  ssl_certificate_key        /var/aegir/platforms/ssl/kw01.net/openssl.key;
  return 301 $scheme://<?php print $this->redirection; ?>$request_uri;
}
<?php endif;?>

<?php endif; ?>

<?php if((!$this->redirection && !empty($custom_domains)) || (!empty($custom_domains) && (strpos($custom_domains, $this->redirection) !== FALSE))) : ?>
# Encryption for letsencrypt
# applied only to customers own domain.
server {
   include       fastcgi_params;
  # Block https://httpoxy.org/ attacks.
  fastcgi_param HTTP_PROXY "";
  fastcgi_param MAIN_SITE_NAME <?php print $this->uri; ?>;
  set $main_site_name "<?php print $this->uri; ?>";
  fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
  fastcgi_param HTTPS on;
<?php
  // If any of those parameters is empty for any reason, like after an attempt
  // to import complete platform with sites without importing their databases,
  // it will break Nginx reload and even shutdown all sites on the system on
  // Nginx restart, so we need to use dummy placeholders to avoid affecting
  // other sites on the system if this site is broken.
  if (!$db_type || !$db_name || !$db_user || !$db_passwd || !$db_host) {
    $db_type = 'mysqli';
    $db_name = 'none';
    $db_user = 'none';
    $db_passwd = 'none';
    $db_host = 'localhost';
  }
?>
  fastcgi_param db_type   <?php print urlencode($db_type); ?>;
  fastcgi_param db_name   <?php print urlencode($db_name); ?>;
  fastcgi_param db_user   <?php print urlencode($db_user); ?>;
  fastcgi_param db_passwd <?php print urlencode($db_passwd); ?>;
  fastcgi_param db_host   <?php print urlencode($db_host); ?>;
<?php
  // Until the real source of this problem is fixed elsewhere, we have to
  // use this simple fallback to guarantee that empty db_port does not
  // break Nginx reload which results with downtime for the affected vhosts.
  if (!$db_port) {
    $db_port = $this->server->db_port ? $this->server->db_port : '3306';
  }
?>
  fastcgi_param db_port   <?php print urlencode($db_port); ?>;
  listen        <?php print "*:{$https_port} {$ssl_args}"; ?>;
  server_name   <?php
    if (!$this->redirection && !empty($custom_domains)) {
      print $custom_domains;
    } else if ($this->redirection) {
      print str_replace('/', '.', $this->redirection);
    }?>;
  root          <?php print "{$this->root}"; ?>;
  ssl                        on;
  ssl_certificate_key        <?php print $https_cert_key; ?>;
<?php if (!empty($https_chain_cert)) : ?>
  ssl_certificate            <?php print $https_chain_cert; ?>;
<?php else: ?>
  ssl_certificate            <?php print $https_cert; ?>;
<?php endif; ?>
<?php print $extra_config; ?>
  include                    <?php print $server->include_path; ?>/nginx_vhost_common.conf;
}

<?php endif; ?>

<?php if((!$this->redirection && !empty($kw01_net_domains)) || (!empty($kw01_net_domains) && (strpos($kw01_net_domains, $this->redirection) !== FALSE))) : ?>
# Encryption for kw01.net domains
server {
  include       fastcgi_params;
  # Block https://httpoxy.org/ attacks.
  fastcgi_param HTTP_PROXY "";
  fastcgi_param MAIN_SITE_NAME <?php print $this->uri; ?>;
  set $main_site_name "<?php print $this->uri; ?>";
  fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
  fastcgi_param HTTPS on;
<?php
  // If any of those parameters is empty for any reason, like after an attempt
  // to import complete platform with sites without importing their databases,
  // it will break Nginx reload and even shutdown all sites on the system on
  // Nginx restart, so we need to use dummy placeholders to avoid affecting
  // other sites on the system if this site is broken.
  if (!$db_type || !$db_name || !$db_user || !$db_passwd || !$db_host) {
    $db_type = 'mysqli';
    $db_name = 'none';
    $db_user = 'none';
    $db_passwd = 'none';
    $db_host = 'localhost';
  }
?>
  fastcgi_param db_type   <?php print urlencode($db_type); ?>;
  fastcgi_param db_name   <?php print urlencode($db_name); ?>;
  fastcgi_param db_user   <?php print urlencode($db_user); ?>;
  fastcgi_param db_passwd <?php print urlencode($db_passwd); ?>;
  fastcgi_param db_host   <?php print urlencode($db_host); ?>;
<?php
  // Until the real source of this problem is fixed elsewhere, we have to
  // use this simple fallback to guarantee that empty db_port does not
  // break Nginx reload which results with downtime for the affected vhosts.
  if (!$db_port) {
    $db_port = $this->server->db_port ? $this->server->db_port : '3306';
  }
?>
  fastcgi_param db_port   <?php print urlencode($db_port); ?>;
  listen        <?php print "*:{$https_port} {$ssl_args}"; ?>;
  server_name   <?php
      if (!$this->redirection && !empty($kw01_net_domains)) {
        print $kw01_net_domains;
      } else if ($this->redirection) {
        print str_replace('/', '.', $this->redirection);
      }?>;
  root          <?php print "{$this->root}"; ?>;
  ssl                        on;
  ssl_certificate            /var/aegir/platforms/ssl/kw01.net/openssl_kw01_nginx.crt;
  ssl_certificate_key        /var/aegir/platforms/ssl/kw01.net/openssl.key;
<?php print $extra_config; ?>
  include                    <?php print $server->include_path; ?>/nginx_vhost_common.conf;
}

<?php endif; ?>

<?php if((!$this->redirection && !empty($churchdesk_com_domains)) || (!empty($churchdesk_com_domains) && (strpos($churchdesk_com_domains, $this->redirection) !== FALSE))) : ?>
# Encryption for churchdes.com domains
server {
  include       fastcgi_params;
  # Block https://httpoxy.org/ attacks.
  fastcgi_param HTTP_PROXY "";
  fastcgi_param MAIN_SITE_NAME <?php print $this->uri; ?>;
  set $main_site_name "<?php print $this->uri; ?>";
  fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
  fastcgi_param HTTPS on;
<?php
  // If any of those parameters is empty for any reason, like after an attempt
  // to import complete platform with sites without importing their databases,
  // it will break Nginx reload and even shutdown all sites on the system on
  // Nginx restart, so we need to use dummy placeholders to avoid affecting
  // other sites on the system if this site is broken.
  if (!$db_type || !$db_name || !$db_user || !$db_passwd || !$db_host) {
    $db_type = 'mysqli';
    $db_name = 'none';
    $db_user = 'none';
    $db_passwd = 'none';
    $db_host = 'localhost';
  }
?>
  fastcgi_param db_type   <?php print urlencode($db_type); ?>;
  fastcgi_param db_name   <?php print urlencode($db_name); ?>;
  fastcgi_param db_user   <?php print urlencode($db_user); ?>;
  fastcgi_param db_passwd <?php print urlencode($db_passwd); ?>;
  fastcgi_param db_host   <?php print urlencode($db_host); ?>;
<?php
  // Until the real source of this problem is fixed elsewhere, we have to
  // use this simple fallback to guarantee that empty db_port does not
  // break Nginx reload which results with downtime for the affected vhosts.
  if (!$db_port) {
    $db_port = $this->server->db_port ? $this->server->db_port : '3306';
  }
?>
  fastcgi_param db_port   <?php print urlencode($db_port); ?>;
  listen        <?php print "*:{$https_port} {$ssl_args}"; ?>;
  server_name   <?php
     if (!$this->redirection && !empty($churchdesk_com_domains)) {
      print $churchdesk_com_domains;
    } else if ($this->redirection) {
      print str_replace('/', '.', $this->redirection);
    } ?>;
  root          <?php print "{$this->root}"; ?>;
  ssl                        on;
  ssl_certificate            /var/aegir/platforms/ssl/churchdesk.com/openssl_churchdesk_nginx.crt;
  ssl_certificate_key        /var/aegir/platforms/ssl/churchdesk.com/openssl.key;

<?php print $extra_config; ?>
  include                    <?php print $server->include_path; ?>/nginx_vhost_common.conf;
}

<?php endif; ?>

<?php endif; ?>

<?php
  // Generate the standard virtual host too.
  include(provision_class_directory('Provision_Config_Nginx_Site') . '/vhost.tpl.php');
?>
